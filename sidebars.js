module.exports = {
  v2: {
    'Aspectos generales': [
      'v2/features/overview_1'
    ],
    'Autenticación': [
      'v2/auth/authentication_1',
      'v2/auth/authentication_2'
    ],
    'Documentos': [
      'v2/documents/create',
      'v2/documents/list',
      'v2/documents/list_by_date_range',
      'v2/documents/show',
      'v2/documents/delete',
      'v2/documents/cert',
      'v2/documents/asn1',
      'v2/documents/status',
      'v2/documents/reminder'
    ],
    'Organización': [
      'v2/organization/create'
    ],
    'Carpeta': [
      'v2/folder/create',
      'v2/folder/documents'
    ],
    'Errores': [
      'v2/errors/error_handling'
    ]
  },
  v3: {
    'Aspectos generales': [
      'v3/introduction'
    ],
    'Tokens': [
      'v3/tokens/jwt'
    ],
    'Estampas de tiempo': [
      'v3/timestamps/index',
      'v3/timestamps/bitcoin',
      'v3/timestamps/nom151'
    ]
  }
};
