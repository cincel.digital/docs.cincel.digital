import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';
import Translate, {translate} from '@docusaurus/Translate';

const features = [
  {
    title: <><Translate>Características</Translate></>,
    imageUrl: 'img/caracteristicas.png',
    description: (
      <>
        
      </>
    ),
    linkUrl: 'v2',
  },
  {
    title: <><Translate>Autenticación</Translate></>,
    imageUrl: 'img/autenticacion.png',
    description: (
      <>
        
      </>
    ),
    linkUrl: 'v2/auth/authentication_1',
  },
  {
    title: <><Translate>Documentos</Translate></>,
    imageUrl: 'img/documentos.png',
    description: (
      <>
        
      </>
    ),
    linkUrl: 'v2/documents/create',
  },
  {
    title: <><Translate>Manejo de errores</Translate></>,
    imageUrl: 'img/manejo-errores.png',
    description: (
      <>
        
      </>
    ),
    linkUrl: 'v2/errors/error_handling',
  },
];

function Feature({imageUrl, title, description, linkUrl}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--3', styles.feature)}>
      <a href={linkUrl}>
        {imgUrl && (
          <div className="text--center">
            <img className={styles.featureImage} src={imgUrl} alt={title} />
          </div>
        )}
        <h3>{title}</h3>
        <p>{description}</p>
      </a>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title="CINCEL API Docs"
      description="CINCEL API Docs. Everything you need to implement the easiest signature software ever.">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title"><Translate>{siteConfig.title}</Translate></h1>
          <p className="hero__subtitle"><Translate>{siteConfig.tagline}</Translate></p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('v2')}>
              <Translate>Comenzar</Translate>
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container features-main">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;
