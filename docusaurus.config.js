module.exports = {
  title: 'CINCEL API Docs',
  tagline: 'Guías de implementación de la API de CINCEL',
  url: 'https://docs.cincel.digital',
  baseUrl: '/',
  favicon: 'img/favicon.ico',
  organizationName: 'CINCEL', // Usually your GitHub org/user name.
  projectName: 'docs', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'CINCEL',
      logo: {
        alt: 'Logo CINCEL',
        src: 'img/cincel-logo.png',
      },
      items: [
        {
          to: '/v2',
          label: 'Firma de Documentos',
          position: 'left',
        },
        {
          to: '/v3',
          label: 'Estampas de Tiempo',
          position: 'left',
        },
        {
          type: 'localeDropdown',
          position: 'left',
        }
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Conócenos',
          items: [
            {
              label: 'Sitio web',
              href: 'https://cincel.digital',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/cincel_it',
            },
          ],
        },
        {
          title: 'Usa CINCEL',
          items: [
            {
              label: 'Iniciar sesión',
              href: 'https://app.cincel.digital',
            },
            {
              label: 'Crea tu cuenta',
              href: 'https://app.cincel.digital/signup',
            },
          ],
        },
        {
          title: 'Más',
          items: [
            {
              label: 'Centro de ayuda',
              href: 'https://ayuda.cincel.digital',
            },
          ],
        },
      ],
      copyright: "Copyright 2022 CINCEL. Todos los derechos reservados.",
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          routeBasePath: '/',
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: 'https://gitlab.com/cincel.digital/docs.cincel.digital/-/edit/master',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  i18n: {
    defaultLocale: 'es',
    locales: ['es', 'en'],
    localeConfigs: {
      es: {
        label: 'Español'
      },
      en: {
        label: 'English'
      }
    }
  }
};
