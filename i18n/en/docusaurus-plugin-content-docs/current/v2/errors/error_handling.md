---
id: error_handling
title: Error handling
---

When interacting with the different API endpoints, a series of errors can occur depending on different situations such as sending data in the wrong format, invalid parameters, etc.

At Cincel we handle our own standard for the possible responses of all endpoints. This response is made up of 3 main variables: ``ok``, ``payload`` and ``errors``.

If the call was successful, the ``ok`` variable will have a value of ``true`` and within ``payload`` you will find the content expected by the call made, which is specified in each of the calls.

Successful response:

```json
{
    "ok": true,
    "payload": {
      ...
    }
}
```

Conversely, if a problem occurs, the ``ok`` variable will have a value of ``false`` and ``errors`` will contain information about the detected error(s).

The error format is defined as follows:

```json
{
    "ok": false,
    "errors":{
      "variable_1": ["error 1","error n"],
      "arreglo_1": {
        "index_elemento": {
          "variable_1_elemento_1":["error 1","error n"]
        }
      }
    }

}
```

Both successful responses and known errors will return a ``200`` status, in case an unexpected error occurs (which is rare), a ``500`` status will be returned.
