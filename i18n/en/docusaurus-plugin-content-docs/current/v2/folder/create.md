---
id: create
title: Create folder
---

In order to create a folder, it is required to do a ``POST`` to ``/folders``, providing the name of the folder.

```bash
curl --location --request POST 'https://sandbox.api.cincel.digital/v1/folders'\
  --header 'x-api-key: my_secret_key' \
  --header 'Content-Type: application/json' \
  --data-raw '{
      "name":"Carpeta de contratos"
  }'
```

Response:

```bash
{
    "ok": true,
    "payload": {
        "name": "Carpeta de contratos",
        "id": 235,
        "owner_type": "User",
        "owner_id": 3,
        "creator_id": 3,
        "created_at": "2021-12-13T09:33:50.985-06:00",
        "updated_at": "2021-12-13T09:33:50.985-06:00"
    }
}

```
