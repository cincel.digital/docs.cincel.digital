---
id: documents
title: Add documents within a folder
---

Moving a document to a folder requires sending a ``PUT`` to ``/documents``, providing the ``id`` of the folder where we want to move the document.

```bash
curl --location --request POST 'https://sandbox.api.cincel.digital/v1/documents/0'\
  --header 'x-api-key: my_secret_key' \
  --header 'Content-Type: application/json' \
  --data-raw '{
      "folder_id":"1"
  }'
```

Response:

```bash {8}
{
    "ok": true,
    "payload": {
        "id": 7963,
        "with_nom151": false,
        "owner_id": 3,
        "owner_type": "User",
        "folder_id": 1,
        "name": "Documento dentro de folder",
        "description": "Descripcion para documento dentro de folders",
        "last_version": "1639410123",
        "creator_id": 3,
        "advanced_signature_version": "cincel_v1",
        "autograph_signature_version": "cincel_v1",
        "created_at": "2021-12-13T09:41:38.279-06:00",
        "updated_at": "2021-12-13T09:43:05.417-06:00",
        "status": "unsigned",
        "active": true,
        "validate_signers_3id": false,
        "blockchain_certs_enabled": false,
        "signing_stage": 1,
        "organization": null,
        "files": [
            {
                "id": 7615,
                "document_id": 7963,
                "name": null,
                "signed_ended_at": null,
                "status": "unsigned",
                "sha256": null,
                "adv_id": null,
                "adv_moment": null,
                "adv_str_end": null,
                "adv_str_now": null,
                "adv_str_constance": null,
                "version": "1639410123",
                "created_at": "2021-12-13T09:42:03.173-06:00",
                "updated_at": "2021-12-13T09:42:03.389-06:00",
                "xml_btc_cert": null,
                "xml_eth_cert": null,
                "blockchain_certs_status": "not_requested",
                "sha256_signed_file": null,
                "url": "https://s3.amazonaws.com/staging.docs.cincel.digital/pZap9ivJFpAzQLZkVZdRpR2Q?response-content-disposition=inline%3B%20filename%3D%22file_16394101237615.pdf%22%3B%20filename%2A%3DUTF-8%27%27file_16394101237615.pdf\u0026response-content-type=application%2Fpdf\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=AKIAQGFN46YARMTOQC6A%2F20211213%2Fus-east-1%2Fs3%2Faws4_request\u0026X-Amz-Date=20211213T154305Z\u0026X-Amz-Expires=432000\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=73cb4c3093c3e3daba97642bd9260a4f71988b6377f17a64c16ab6e528beee31",
                "signed_file_url": null,
                "evidence_doc_url": null,
                "comments": [],
                "signers": [
                    {
                        "id": 9196,
                        "file_id": 7615,
                        "signature_type": null,
                        "created_at": "2021-12-13T09:42:12.368-06:00",
                        "updated_at": "2021-12-13T09:42:12.368-06:00",
                        "status": "unsigned",
                        "email": "pruebas@cincel.digital",
                        "hash_to_sign": null,
                        "user": {
                            "email": "pruebas@cincel.digital",
                            "name": "Luis Parada",
                            "user_id": 3
                        },
                        "user_id": 3
                    }
                ]
            }
        ],
        "invitations": [
            {
                "id": 9202,
                "invite_name": "Luis Parada",
                "invite_email": "pruebas@cincel.digital",
                "document_id": 7963,
                "status": "accepted",
                "created_at": "2021-12-13T09:42:12.351-06:00",
                "updated_at": "2021-12-13T09:42:12.351-06:00",
                "stage": 1,
                "customer_id_3id": null,
                "validation_status_3id": "pending",
                "allowed_signature_types": "all",
                "requested_documents_status": "pending",
                "requested_documents_required": false,
                "validate_kyc": false,
                "requested_documents": []
            }
        ]
    }
}
```
