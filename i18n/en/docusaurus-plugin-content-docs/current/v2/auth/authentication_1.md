---
id: authentication_1
title: How to get your API keys
---

### Previous requirements
- Have an account in [CINCEL](https://sandbox.app.cincel.digital/).
- Know your CINCEL email and password.

### 1. Generate a JsonWebtoken

Generate a JWT as follows, replacing the email and password with those of your CINCEL account.

```bash
curl 'https://sandbox.api.cincel.digital/v1/login' \
-H 'Content-Type: application/json;charset=utf-8' \
--data-raw '{"email":"user@example.com","password":"contraseña"}' 
```

You will get a response like below, `token` contains the JWT.

```javascript
{
  "token": "eyJhbGciOiJIUzI1NiJ9._._",
  "expiration": 1603915806
}
```

### 2. Get your APIKEY

Using the JWT you got in the previous step, send the following GET request to our API, to get your APIKEY:

```bash
curl 'https://sandbox.api.cincel.digital/v1/users/api_key' \
-H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9._._'
-H 'Content-Type: application/json;charset=utf-8' \
```

The response body contains your APIKEY.

```javascript
{
  "ok": true,
  "payload": {
    "api_key": "Q-f89hj9s08hsd_u"
  }
}
```

To obtain your authentication keys, you must first log in to your [cincel](https://app.cincel.digital/login) account.

You must navigate to the "My Profile" section in the upper right corner, once inside look for a button that says "Generate API Keys". It will guide you through the process to obtain the keys with which you can consume our API.
