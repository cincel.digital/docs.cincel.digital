---
id: authentication_2
title: Authentication method
---

In order to authenticate you must include the API KEY in the X-Api-Key header as follows:

```
X-Api-Key = "XX-XXX-XXXXXX"
```
