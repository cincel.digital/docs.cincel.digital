---
id: overview_1
title: Introduction
sidebar_label: API v2
slug: /v2
---

Our API v2 is implemented inspired by the API REST scheme, so all calls exchange messages in JSON format.

With the use of our API v2 you can perform the following tasks:

- Creation of documents.
- Elimination of documents.
- Get the list of your documents.
- Get a specific document.
- Send signature reminders.

If you want to consult the API v3 guides, you can review the [API v3 documentation](/v3).
