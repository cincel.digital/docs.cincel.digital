---
id: create
title: Create organization
---

In order to create an organization, it is necessary to do a ``POST`` to ``/organizations``, providing the name of the organization and a base64 image, with a .png extension.

```bash
curl --location --request POST 'https://sandbox.api.cincel.digital/v1/documents'\
  --header 'x-api-key: my_secret_key' \
  --header 'Content-Type: application/json' \
  --data-raw '{
      "name":"Organización Cincel",
      "imagotype":"Cdata:image/png;base64, iVbORw0KGgoAAAANSUhEUgAAAN///..",
  }'
```
