---
id: asn1
title: Obtain NOM151 certificate
---

In order to access all the properties of a document, as well as its list of versions and signers, it is necessary to obtain it through the document id, performing a ``GET`` to ``/documents/:document_id``.

The attribute we need to check to perform the ASN1 file download process is ``adv_str_constance`` within the `file {}` properties.

If this variable has values we can proceed to generate the file, for this we must add the following value ``data:application/octet-stream;base64,`` plus the value found in ``adv_str_constance``. Once having the complete string, the extension of the file for download is attached, which is .asn1, leaving a final string like this:

``data:application/octet-stream;base64, + adv_str_constance + asn1``

```bash
curl  --location --request GET 'https://sandbox.api.cincel.digital/v1/documents/0' \
  --header 'x-api-key: my_secret_key' \
  --header 'Content-Type: application/json' \
```

Response:

```bash {35}
{
    "ok": true,
    "payload": {
        "id": 6310,
        "with_nom151": true,
        "owner_id": 3,
        "owner_type": "User",
        "folder_id": null,
        "name": "asd",
        "description": "asd",
        "last_version": "1622672918",
        "creator_id": 3,
        "advanced_signature_version": "seguridata_v1",
        "autograph_signature_version": "seguridata_v1",
        "created_at": "2021-06-02T17:28:34.784-05:00",
        "updated_at": "2021-06-10T09:46:40.180-05:00",
        "status": "signed",
        "active": true,
        "validate_signers_3id": false,
        "blockchain_certs_enabled": true,
        "signing_stage": 1,
        "organization": null,
        "files": [
            {
                "id": 5986,
                "document_id": 6310,
                "name": null,
                "signed_ended_at": "2021-06-10T09:46:40.167-05:00",
                "status": "signed",
                "sha256": "04C67C37510CB5D6942C207CDF6000E1F10D1B4A686F5945856E5A4DB8B3B973",
                "adv_id": null,
                "adv_moment": null,
                "adv_str_end": null,
                "adv_str_now": null,
                "adv_str_constance": "MIIINDADAgEAMIIIKwYJKoZIhvcNAQcCoIIIHDCCCBgCAQMxDz...",
                "version": "1622672918",
                "created_at": "2021-06-02T17:28:38.807-05:00",
                "updated_at": "2021-06-10T09:46:41.082-05:00",
                "xml_btc_cert": null,
                "xml_eth_cert": null,
                "blockchain_certs_status": "requested",
                "sha256_signed_file": "eb82025d1d4e6fc86f66a9ea1b53ff5e46b63c73ebe673193d8387ae36a5f1c1",
                "url": "https://s3.amazonaws.com/staging.docs.cincel.digital/jeFZfB3......",
                "signed_file_url": "https://s3.amazonaws.com/staging.docs.cincel.digital/4f8xRnr7etT3U1...",
                "evidence_doc_url": null,
                "comments": [],
                "signers": [
                    {
                        "id": 7030,
                        "file_id": 5986,
                        "signature_type": "advanced",
                        "created_at": "2021-06-02T17:28:39.480-05:00",
                        "updated_at": "2021-06-10T09:46:39.117-05:00",
                        "status": "signed",
                        "email": "pruebas@cincel.digital",
                        "hash_to_sign": "186FAFAAFCF9A11BB178939567D596DC04B726979998610973FF33C8CCCE9208",
                        "user": {
                            "email": "fpruebas@cincel.digital",
                            "name": "Angel Spinoso",
                            "user_id": 3
                        },
                        "user_id": 3
                    }
                ]
            }
        ],
        "invitations": [
            {
                "id": 7035,
                "invite_name": "Angel Spinoso",
                "invite_email": "pruebas@cincel.digital",
                "document_id": 6310,
                "status": "accepted",
                "created_at": "2021-06-02T17:28:39.452-05:00",
                "updated_at": "2021-06-02T17:28:39.452-05:00",
                "stage": 1,
                "customer_id_3id": null,
                "validation_status_3id": "pending",
                "allowed_signature_types": "all",
                "requested_documents_status": "pending",
                "requested_documents_required": false,
                "validate_kyc": false,
                "requested_documents": []
            }
        ]
    }
}
```
