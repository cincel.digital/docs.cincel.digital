---
id: create
title: Create document
---

Creating a document requires a ``POST`` to ``/documents``, providing the base64 pdf file, a title, the list of signers, and a number of other optional parameters.

Example:

```bash
curl --location --request POST 'https://sandbox.api.cincel.digital/v1/documents'\
  --header 'x-api-key: my_secret_key' \
  --header 'Content-Type: application/json' \
  --data-raw '{
      "name":"Contrato 1",
      "description":"Contrato para adquisición de bienes",
      "invitations_attributes":[
        {
          "invite_email":"juan@example.com",
          "invite_name":"Juan Pérez",
          "message":"Por favor firma el contrato de adquisición de bienes.",
          "allowed_signature_types": "all",
          "stage": 2
        },	
        {
          "invite_email":"ana@example.com",
          "invite_name":"Ana Pérez",
          "message":"Por favor firma el contrato de adquisición de bienes.",
          "allowed_signature_types": "autograph",
          "stage": 1
        },	
        {
          "invite_email":"cecilia@example.com",
          "invite_name":"Cecilia Pérez",
          "message":"Por favor firma el contrato de adquisición de bienes.",
          "allowed_signature_types": "advanced",
          "stage": 1
        }
      ],
      "base64_file":"data:application/pdf;base64,JVBERi0xLjMKJf////..."
  }'
```

Keep in mind that when you use your productive key you will be limited by the number of documents you have purchased and the storage space you have available in your account.

Creating a document will not decrease your counter of available documents immediately, but until the signing process has started when one of the signers signs the document.
