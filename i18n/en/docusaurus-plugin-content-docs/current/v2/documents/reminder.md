---
id: reminder
title: Send reminders to signers
---

To obtain the identifier of the invitations, you must obtain the properties of the document. To do this, a ``GET`` request is made to ``/documents/:document_id`` using its ``id``.

Example:

```bash
curl  --location --request GET 'https://sandbox.api.cincel.digital/v1/documents/0' \
      --header 'x-api-key: my_secret_key'
```

Response:

```bash
{
    "ok": true,
    "payload": {
        "id": 21,
        "owner_id": 2,
        "owner_type": "User",
        "folder_id": null,
        "name": "Contrato 1",
        "description": "Contrato de adquisición de bienes",
        "last_version": "1596125425",
        "created_at": "2020-01-01T00:00:00.000-05:00",
        "updated_at": "2020-01-01T00:00:00.000-05:00",
        "status": "signed",
        "organization": null,
        "files": [
            {
                "id": 0,
                "document_id": 0,
                "name": null,
                "signed_ended_at": "2020-01-01T00:00:00.000-05:00",
                "status": "signed",
                "sha256": "CD4E31AC2288FDAA752E37FAB847E53FC4D0734010F193580C2790C9660D6A90",
                "version": "1596125425",
                "created_at": "2020-01-01T00:00:00.000-05:00",
                "updated_at": "2020-01-01T00:00:00.000-05:00",
                "url": "URL_DEL_DOCUMENTO_PDF_ORIGINAL",
                "signed_file_url": "URL_DEL_DOCUMENTO_PDF_FIRMADO",
                "comments": [
                    {
                        "title": "Mi comentario",
                        "text": "comentario 1",
                        "created_at": "2020-01-01T00:00:00.000-05:00",
                        "user": {
                            "name": "Juan Pérez",
                            "email": "example@mail.com"
                            "id": null,
                        }
                    }
                ],
                "signers": [
                    {
                        "id": 0,
                        "file_id": 0,
                        "signature_type": "advanced",
                        "created_at": "2020-01-01T00:00:00.000-05:00",
                        "updated_at": "2020-01-01T00:00:00.000-05:00",
                        "status": "signed",
                        "email": "example@mail.com",
                        "user": {
                            "email": "example2@mail.com",
                            "name": "Pedro Pérez",
                            "user_id": 0
                        }
                    }
                ]
            }
        ],
        "invitations": [
            {
                "id": 71061,
                "invite_name": "Juan Pérez",
                "invite_email": "example@mail.com",
                "document_id": 0,
                "status": "accepted",
                "created_at": "2020-01-01T00:00:00.000-05:00",
                "updated_at": "2020-01-01T00:00:00.000-05:00",
                "user": {
                    "user_id": null,
                    "name": "Juan Pérez",
                    "email": "example@mail.com"
                },
            }
        ]
    }
}
```

### ``Document['invitations']``

The list of signers that were included in the creation of the document is represented by the ``invitations`` property which is an array of objects, with the name, email of the invited signer and an ``id`` identifier.

To send a reminder to a signer, send the following ``POST`` request to ``https://sandbox.api.cincel.digital/v1/invitations/idInvitation/reminder``.

Example:

```bash
curl  --location --request GET 'https://sandbox.api.cincel.digital/v1/invitations/71061/reminder' \
      --header 'x-api-key: my_secret_key'
```

The successful response would be status code 204.
