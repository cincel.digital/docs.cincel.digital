---
id: list
title: List of documents
---

To get the list of your documents, just do a ``GET`` to ``/documents``, which will return an array with the main properties of each document. It is also possible to pass some optional properties to it to get only the documents in a folder or organization.

Example:

```bash
curl  --location --request GET 'https://sandbox.api.cincel.digital/v1/documents?organization_id=0&folder_id=0' \
      --header 'x-api-key: my_secret_key'
```

Response:

```json
{
    "ok": true,
    "payload": [
        {
            "id": 0,
            "owner_id": 0,
            "owner_type": "User",
            "folder_id": 0,
            "name": "Contrato 1",
            "description": "Contrato para adquisición de bienes",
            "last_version": "1596141864",
            "created_at": "2020-01-01T00:00:00.000-05:00",
            "updated_at": "2020-01-01T00:00:00.000-05:00",
            "status": "unsigned",
            "organization": {
                "id": 0,
                "name": "Cincel"
            }
        }
        ...
    ]
}
```

Note that there will be documents that do not belong to any folder or organization, so the ``organization`` and ``folder_id`` properties can be ``null``, as well as the other optional properties.
