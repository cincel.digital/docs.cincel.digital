---
id: delete
title: Delete document
---

To delete a document, just do a ``DELETE`` to ``/documents/:document_id``.

Example:

```bash
curl  --location --request GET 'https://sandbox.api.cincel.digital/v1/documents/0' \
      --header 'x-api-key: my_secret_key'
```

Response:

```json
{
  "ok": true,
  "payload": {}
}
```

Keep in mind that once the document is deleted you will not be able to recover it and both you and the signers will not be able to access it.
