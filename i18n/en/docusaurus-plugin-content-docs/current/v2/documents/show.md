---
id: show
title: Get document
---

To be able to access all the properties of a document, as well as its list of versions and signers, it is necessary to obtain it through its ``id``, performing a ``GET`` to ``/documents/:document_id``.

Example:

```bash
curl  --location --request GET 'https://sandbox.api.cincel.digital/v1/documents/0' \
      --header 'x-api-key: my_secret_key'
```

Response:

```bash
{
    "ok": true,
    "payload": {
        "id": 21,
        "owner_id": 2,
        "owner_type": "User",
        "folder_id": null,
        "name": "Contrato 1",
        "description": "Contrato de adquisición de bienes",
        "last_version": "1596125425",
        "created_at": "2020-01-01T00:00:00.000-05:00",
        "updated_at": "2020-01-01T00:00:00.000-05:00",
        "status": "signed",
        "organization": null,
        "files": [
            {
                "id": 0,
                "document_id": 0,
                "name": null,
                "signed_ended_at": "2020-01-01T00:00:00.000-05:00",
                "status": "signed",
                "sha256": "CD4E31AC2288FDAA752E37FAB847E53FC4D0734010F193580C2790C9660D6A90",
                "version": "1596125425",
                "created_at": "2020-01-01T00:00:00.000-05:00",
                "updated_at": "2020-01-01T00:00:00.000-05:00",
                "url": "URL_DEL_DOCUMENTO_PDF_ORIGINAL",
                "signed_file_url": "URL_DEL_DOCUMENTO_PDF_FIRMADO",
                "comments": [
                    {
                        "title": "Mi comentario",
                        "text": "comentario 1",
                        "created_at": "2020-01-01T00:00:00.000-05:00",
                        "user": {
                            "name": "Juan Pérez",
                            "email": "example@mail.com"
                            "id": null,
                        }
                    }
                ],
                "signers": [
                    {
                        "id": 0,
                        "file_id": 0,
                        "signature_type": "advanced",
                        "created_at": "2020-01-01T00:00:00.000-05:00",
                        "updated_at": "2020-01-01T00:00:00.000-05:00",
                        "status": "signed",
                        "email": "example@mail.com",
                        "user": {
                            "email": "example2@mail.com",
                            "name": "Pedro Pérez",
                            "user_id": 0
                        }
                    }
                ]
            }
        ],
        "invitations": [
            {
                "id": 0,
                "invite_name": "Juan Pérez",
                "invite_email": "example@mail.com",
                "document_id": 0,
                "status": "accepted",
                "created_at": "2020-01-01T00:00:00.000-05:00",
                "updated_at": "2020-01-01T00:00:00.000-05:00",
                "user": {
                    "user_id": null,
                    "name": "Juan Pérez",
                    "email": "example@mail.com"
                },
            }
        ]
    }
}
```

As we can see, the document is made up of a series of objects that represent its version list, its list of invitations to sign, comments and signers of each version. We will focus only on the most relevant.

### ``Document['files']``

There is the possibility of creating new versions for a document, in case it contains errors and a new PDF needs to be uploaded, without having to invite the signatories again. Within the API we refer to those versions as ``files``. It is worth mentioning that the creation of new versions is only allowed for documents that have ``unsigned`` status, that is, that none of the signatories has previously signed. Versioning is currently only available within the CINCEL platform.

The ``files`` property is an array of objects where each one contains within the url of the original pdf and the signed pdf. As well as the list of comments made by the different users who were invited to sign the document and which differ between each version of the document. At the moment only registered users are allowed to comment within CINCEL.

To identify the latest version of a document, we can rely on the document's ``last_version`` variable, which must match the ``version`` property of one of the ``files`` elements.

### ``Document['invitations']``

The list of signers that were included in the creation of the document is represented by the ``invitations`` property which is an array of objects, with the name and email of the invited signer. As well as information about your user, in case the email belongs to a user registered in CINCEL.
