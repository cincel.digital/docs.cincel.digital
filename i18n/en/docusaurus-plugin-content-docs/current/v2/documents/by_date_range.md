---
id: list_by_date_range
title: List of documents by date range
---

To get the list of your documents filtered by a date range (``from_date`` and ``to_date``) and status (``status``), just do a ``GET`` to ``/documents /by_date_range``, which will return an array with the main properties of each document. It is also possible to pass some optional properties to it to get only the documents in a folder or organization.

This is what can accept status:

```js
status = ["signed","unsigned","partially_signed"]
```

Example:

```bash
curl  --location --request GET 'https://sandbox.api.cincel.digital/v1/documents/by_date_range?status=signed&to_date=2022-12-30' \
      --header 'x-api-key: my_secret_key' \
      -d status=signed
      -d from_date=2021-01-01
      -d to_date=2022-12-30
```

Response:

```json
{
    "ok": true,
    "payload": [
        {
            "id": 15897,
            "name": "Document",
            "description": "Document description",
            "created_at": "2021-01-20T20:30:25.229-06:00",
            "status": "signed",
            "unsigned_last_version_url": "https://s3.amazonaws.com/docs.cincel.digital/...",
            "signed_last_version_url": "https://s3.amazonaws.com/docs.cincel.digital/...",
            "last_version_signed_ended_at": "2021-01-20T20:30:41.586-06:00",
            "organization_id": null,
            "organization_name": null,
            "created_by_id": 537,
            "created_by_name": "Servicios Cincel",
            "created_by_email": "servicios@cincel.digital"
        }
        ...
    ]
}
```

Note that there will be documents that do not belong to any folder or organization, so the ``organization`` and ``folder_id`` properties can be ``null``, as well as the other optional properties.
