---
id: jwt
title: JWT token generation
---

For all v3 functionality of the API it is necessary to request an authentication token (jwt).

This requires the credentials used to access [app.cincel.digital](https://app.cincel.digital/).

:::info
JWT tokens generated from v3 are NOT valid to access the v2 API. If you need to generate a token to access the v2 API, please visit the [v2 guide](/v2/auth/authentication_2).
:::

## URL

```
GET https://sandbox.api.cincel.digital/v3/tokens/jwt
```

## Authentication

- Basic Auth

## Response Codes

| Code | Description                                                                                 | Content-Type     |
|:----:|:-------------------------------------------------------------------------------------------:|:----------------:|
| 200  | OK. The request was made correctly.                                                         | plain/text       |
| 400  | Wrong request. There was an error in the request body.                                      | application/json |
| 401  | Not authorized. The request could not be completed due to incorrect or missing credentials. | application/json |

## Example

Request

```
curl --location --request GET 'https://sandbox.api.cincel.digital/v3/tokens/jwt' \
--header 'Authorization: Basic dGVzdGluZ0BjaW5jZWwuZGlnaXRhbDowMTkyODM3NDY1'
```

Response

```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjk5OTk5OTk5OTk5OSwic2NvcGUiOltdLCJpYXQiOjE2Mz
```
