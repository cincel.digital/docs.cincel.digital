---
title: Bitcoin timestamps
sidebar_label: Bitcoin
---

Bitcoin timestamps provide an additional level of security for document signing as it guarantees that the signed file has not been altered as well as provides evidence that it existed at a certain time in the past.

The Bitcoin blockchain provides a decentralized, anonymous, immutable, and verifiable public ledger.

## Generation of Bitcoin timestamps

The Bitcoin timestamp generation service is asynchronous. When you request the timestamp of a document, CINCEL waits and saves it in the background so that you can consult it later, without having to keep the socket open waiting for the initial response.

The Bitcoin stamps of each day are generated around 18:00 Central Mexico time. Timestamps requested after this time will be completed the following day.

Once the certificate is generated, it is always possible to retrieve it again knowing the hash. No new certificates are generated if one already exists for the given hash. This certificate recovery does not require providing credentials and does not consume credits. Credits are only consumed for each new certificate issued (requires a completely new hash).

Any digital file with a Bitcoin stamp can be verified [here](https://blockchain.cincel.digital/). Here you can check if the digital file has suffered any type of manipulation or if its integrity is still guaranteed, as well as the time it was stamped on the Bitcoin blockchain.

The CINCEL Bitcoin timestamp is available in XML format and PDF format.

### Previous requirements

To be able to use the Bitcoin service it is necessary previously:

- Have obtained your access token for the CINCEL API v3 or have your access keys for app.cincel.digital
- Calculate the hash (SHA256) of the digital file you want to create the Bitcoin timestamp for
- Credits available in your CINCEL account

:::tip
The same timestamp credit entitles you to generate all the timestamps that CINCEL offers, at no additional cost:

- Bitcoin timestamp
- Ethereum timestamp
- NOM151 timestamp
:::

### Hash characteristics

- Created using the SHA256 algorithm
- Hexadecimal encoding
- Length of 32 hexadecimal characters

:::info
SHA256 hash example:
`2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e`
:::

### URL

```
GET https://sandbox.api.cincel.digital/v3/timestamps/{hash}.bitcoin
```

### Parameters

| Parameter          | Datatype | Description                                                                 |
|:------------------:|:--------:|:---------------------------------------------------------------------------:|
| _hash_ (requerido) | _string_ | El hash SHA256 de tu archivo.<br />Exactamente 32 caracteres hexadecimales. |

### Authentication

- JWT Bearer Token (optional) (preferred)
- Basic Auth (optional)

### Response Codes

| Code | Description                                                                                        | Content-Type     |
|:------:|:------------------------------------------------------------------------------------------------:|:----------------:|
| 200    | OK. The request was made correctly.                                                              | application/xml  |
| 202    | Accepted. The request was accepted correctly.                                                    | application/json |
| 400    | Wrong request. There was an error in the request body.                                           | application/json |
| 401    | Not authorized. The request could not be completed due to incorrect or missing credentials.      | application/json |
| 402    | Payment required. The user does not have enough credits to make the request.                     | application/json |
| 404    | Not found. The provided hash certificate was not found and the user did not provide credentials. | application/json |
| 500    | Internal error. An internal error occurred.                                                      | application/json |
| 502    | Bad gateway. Unexpected communication error.                                                     | application/json |

## Example (XML format)

cURL

``` bash
curl \
https://sandbox.api.cincel.digital/v3/timestamps/2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e.bitcoin
```

Response (200)

``` xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<node value="5e92ec09501a5d39e251a151f84b5e2228312c445eb23b4e1de6360e27bad54b" type="key">
    <left value="7811e3130908fd2678eb2dd3928d245db7f3ed578c21f2ae4fbe680424dc735e" type="mesh">
        <left value="61f48c118002c0e7681e425a3b0e2396475fe0d037ebb0360231b95c2fd60c2f" type="mesh">
            <left value="54280d75669fb9f3ff976c66d03daefa02b691f1aa25558a85f86a5c60961c69" type="mesh">
                <left value="43e89c93c4247511a6eabfe24dfcd331453e04bdde13e688865bf73c3243280a" type="mesh"/>
                <right value="6c127084785466791190c1b4673635274d21e065b9ad9c70d246ef41ba960220" type="mesh">
                    <left value="34789458c3010230cef61b6053626ee25f1d79762a76de3517ec5ac2e76ae2e9" type="mesh"/>
                    <right value="f375cd66a5552b189a2bfe5f8be433236ff6829dea7bca710e611e98bc7baf0e" type="mesh">
                        <left value="5e8d9839b88965971e8f8eb06d2d6bf2f72f75b16100ea175350179c6a82807e" type="mesh">
                            <left value="55ed3f4915ab9f93e385f655a2c58c62a5fa2d0e7a4b741263102184e2b7a94f" type="mesh">
                                <left value="c04c5bb97b65d009582feb32d88498880216aee8e3bda0c5a0f4ba12244aedb0" type="mesh"/>
                                <right value="0bee8d27b7fd9129df599166751af1a9e82bf4e7ac91771f9c15ebf0cb05d74d" type="mesh">
                                    <left value="88369cb8594af97fb2ee465d5dc3a01e1e23924b8929faca6fdd7293f97e92be" type="mesh">
                                        <left value="d7ed34b6b74056df3cfb2836bfd55c4cd27b5237ad4a6641cf7003d21be76c88" type="mesh"/>
                                        <right value="af68501bb80cca4c0bc7d4df6aa2a0c347712e1f2cb1b687a0467e2e9aaf545e" type="mesh">
                                            <left value="debd8f8a4aa7af602faa9e8e08f5773c6decc29c9f65e80d70ee90a993170f69" type="mesh">
                                                <left value="8a477ae8ff3b601e709321b17f00117159a7efd861abc74286f7ee6479fe46ce" type="mesh">
                                                    <left value="6a2368efa74c641f2f32d6be8f90a7b6ab5a3235d816faf4034d53d49ce5537c" type="mesh"/>
                                                    <right value="2796b0a5986fe8f497057da457cdcf51ab9fc0ffb996445434bd08e2257b29e8" type="mesh">
                                                        <left value="2c5838c92697fe3d5b8aa14e8f67f6fd2ff4c9e12d536332f57434ebcf0f0196" type="mesh"/>
                                                        <right value="2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e" type="hash"/>
                                                    </right>
                                                </left>
                                                <right value="3d2466ee2746f818708f8f396649e0cc4d13738248e8ee051a767474ac2f87d3" type="mesh"/>
                                            </left>
                                            <right value="130921ae3e04ea13d237a9ed09e33c18574068e8db52e3d7d3d4855d7df3b3ce" type="mesh"/>
                                        </right>
                                    </left>
                                    <right value="8a5d691e559e95448c9aab2d92ba6d58ec9f814ee345e10aee81cf47790d7c2e" type="mesh"/>
                                </right>
                            </left>
                            <right value="55562de568950252020816b6204cbc76820c7c9874c2a4672c99437d99068cf5" type="mesh"/>
                        </left>
                        <right value="f5fef74b0d216ca6221b024cc5f53d5aee7c9bc33f7289c529f465a0ff2969ea" type="mesh"/>
                    </right>
                </right>
            </left>
            <right value="584d06e48bbfe9c5319495b7b3be9eaf7e11aa546ae777575e7e422aafa68fcd" type="mesh"/>
        </left>
        <right value="bf11a94cc0028fe2e0fe476d897da3417cc72777e523d6f0c030c5141bbeb75b" type="mesh"/>
    </left>
    <right value="a62eedb07080ce5a21ad26230bcd50ef37cac8cca43a2f1d946db2d1d47e1f94" type="mesh"/>
</node>
```

## Generation of PDF certificates for Bitcoin

In addition to the certificate in XML format, you can generate a certificate in PDF format that includes all the data of your timestamp. Both certificates contain the same information; CINCEL offers you both formats for your convenience (for example, the PDF certificate is more attractive, while the XML certificate is much more compact).

### URL

```
GET https://sandbox.api.cincel.digital/v3/timestamps/{hash}.pdf
```

### Response Codes

| Code | Description                                                                                        | Content-Type     |
|:------:|:------------------------------------------------------------------------------------------------:|:----------------:|
| 200    | OK. The request was made correctly.                                                              | application/pdf  |
| 202    | Accepted. The request was accepted correctly.                                                    | application/json |
| 400    | Wrong request. There was an error in the request body.                                           | application/json |
| 401    | Not authorized. The request could not be completed due to incorrect or missing credentials.      | application/json |
| 402    | Payment required. The user does not have enough credits to make the request.                     | application/json |
| 404    | Not found. The provided hash certificate was not found and the user did not provide credentials. | application/json |
| 500    | Internal error. An internal error occurred.                                                      | application/json |
| 502    | Bad gateway. Unexpected communication error.                                                     | application/json |

## Example (PDF format)

cURL

``` bash
curl \
https://sandbox.api.cincel.digital/v3/timestamps/2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e.pdf
```

Response (200)

![Blockchain Certificate Example part 1](/img/cert-example-pdf-1.jpg "Blockchain Certificate Example part 1")
![Blockchain Certificate Example part 2](/img/cert-example-pdf-2.jpg "Blockchain Certificate Example part 2")
