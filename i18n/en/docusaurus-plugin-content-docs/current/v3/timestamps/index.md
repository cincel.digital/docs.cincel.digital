---
title: Timestamps
sidebar_label: General description
slug: /v3/timestamps
---

At CINCEL we offer the digital file stamping service on the [Bitcoin](/v3/timestamps/bitcoin) blockchain and stamping [NOM 151](/v3/timestamps/nom151).

In order to use the time stamp service it is necessary that:

- Obtain your access token for the CINCEL API v3 or have your CINCEL email and password
- Calculate the hash (SHA256) of the digital file you want to stamp
- Have available credits in your CINCEL account

:::info
SHA256 hash example:
`2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e`
:::

### URL

```
GET https://sandbox.api.cincel.digital/v3/timestamps/{hash}
```

### Parameters

| Parameter         | Datatype | Description                                                           |
|:-----------------:|:--------:|:---------------------------------------------------------------------:|
| _hash_ (required) | _string_ | The SHA256 hash of your file.<br />Exactly 32 hexadecimal characters. |

### Authentication

- Bearer Token (optional)
- Basic Auth (optional)

### Response Codes

| Code | Description                                                                                   | Content-Type     |
|:------:|:-------------------------------------------------------------------------------------------:|:----------------:|
| 300    | Multiple choices. A list of generated timestamps.                                           | application/json |
| 400    | Wrong request. There was an error in the request body.                                      | application/json |
| 401    | Not authorized. The request could not be completed due to incorrect or missing credentials. | application/json |
| 500    | Internal error. An internal error occurred.                                                 | application/json |

## Example

cURL

``` bash
curl \
https://sandbox.api.cincel.digital/v3/timestamps/2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e
```

Response (300)

``` bash
< HTTP/2 300
⋮
< location: 2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e.bitcoin
```

``` json
[{
    "type": "bitcoin",
    "xml": "2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e.bitcoin",
    "pdf": "2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e.pdf"
},
{
    "type": "ethereum",
    "xml": "2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e.ethereum"
},
{
    "type": "nom151",
    "asn1": "2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e.asn1"
}]
```

:::tip
The same timestamp credit entitles you to generate all the timestamps that CINCEL offers, at no additional cost:

- [Bitcoin timestamp](/v3/timestamps/bitcoin)
- Ethereum timestamp
- [NOM 151 timestamp](/v3/timestamps/nom151)
:::
