---
id: introduction
title: CINCEL v3
sidebar_label: Introduction
slug: /v3
---

CINCEL API v3 is designed based on the [RESTful API](https://restfulapi.net/) schema. This allows third-party integrations to be done faster and more efficiently by just following REST patterns.

Currently, with API v3 you can perform the following tasks:

- Generation of access tokens
- Generation of NOM-151 certificates
- Generation of blockchain time stamps
- Generation of blockchain certificates (PDF)

:::tip
If you want to perform any other option not mentioned here, we invite you to consult the [API v2 documentation](/v2).
:::
