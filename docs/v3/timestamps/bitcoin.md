---
title: Estampas de tiempo Bitcoin
sidebar_label: Bitcoin
---

Las estampas de tiempo Bitcoin proporcionan un nivel adicional de seguridad para la firma de documentos pues garantiza que el archivo firmado no ha sido alterado así como proporciona evidencia de que el mismo existió en determinado momento del pasado.

La blockchain de Bitcoin proporciona un registro público descentralizado, anonimo, inmutable y verificable.

## Generación de estampas de tiempo Bitcoin

El servicio de generación de estampa de tiempo Bitcoin es asíncrono. Cuando solicitas la estampa de tiempo de un documento, CINCEL queda a la espera y la guarda en segundo plano para que posteriormente puedas consultarla, sin tener que mantener abierto el socket a espera de la respuesta inicial.

Las estampas Bitcoin de cada día se generan al rededor de las 18:00 horario del Centro de México. Las estampas de tiempo solicitadas después de este horario se completarán al día siguiente.

Una vez se genera el certificado, siempre es posible volver a recuperarlo conociendo el hash. No se generan nuevos certificados si ya existe uno para el determinado hash. Esta recuperación del certificado no requiere proporcionar credenciales ni consume creditos. Solo se consumen créditos por cada certificado nuevo emitido (require un hash totalmente nuevo).

Cualquier archivo digital con estampa Bitcoin puede ser verificado [aquí](https://blockchain.cincel.digital/). Aquí se puede verificar si el archivo digital ha sufrido algún tipo de manipulación o si su integridad sigue garantizada, así como el tiempo en que fue estampado en la blockchain de Bitcoin.

La estampa de tiempo Bitcoin de CINCEL está disponible en formato XML y formato PDF.

### Requerimientos previos

Para poder utilizar el servicio de Bitcoin es necesario previamente:

- Haber obtenido tu token de acceso para la API v3 de CINCEL o contar con tus claves de acceso para app.cincel.digital
- Calcular el hash (SHA256) del archivo digital al que deseas crearle la estampa de tiempo Bitcoin
- Créditos disponibles en tu cuenta CINCEL

:::tip
El mismo crédito de estampa de tiempo te da derecho a generar todas las estampas de tiempo que ofrece CINCEL, sin costo adicional:

- Estampa de tiempo Bitcoin
- Estampa de tiempo Ethereum
- Estampa de tiempo NOM151
:::

### Caracteristicas del hash

- Creado utilizando el algoritmo SHA256
- Codificación hexadecimal
- Longitud de 32 caracteres hexadecimales

:::info
Ejemplo de hash SHA256:
`2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e`
:::

### URL

```
GET https://sandbox.api.cincel.digital/v3/timestamps/{hash}.bitcoin
```

### Parámetros

| Parámetro          | Tipo de Dato | Descripción                                                                 |
|:------------------:|:------------:|:---------------------------------------------------------------------------:|
| _hash_ (requerido) | _string_     | El hash SHA256 de tu archivo.<br />Exactamente 32 caracteres hexadecimales. |

### Autenticación

- JWT Bearer Token (opcional) (preferido)
- Basic Auth (opcional)

### Códigos de Respuesta

| Código | Descripción                                                                                                   | Content-Type     |
|:------:|:-------------------------------------------------------------------------------------------------------------:|:----------------:|
| 200    | OK. La petición fue realizada correctamente.                                                                  | application/xml  |
| 202    | Aceptado. La petición fue admitida correctamente.                                                             | application/json |
| 400    | Petición erronea. Hubo un error en el cuerpo de la petición.                                                  | application/json |
| 401    | No autorizado. La petición no se pudo completar por credenciales incorrectas o no proporcionadas.             | application/json |
| 402    | Pago requerido. El usuario no cuenta con creditos suficientes para realizar la petición.                      | application/json |
| 404    | No encontrado. No se encontró el certificado del hash proporcionado y el usuario no proporciono credenciales. | application/json |
| 500    | Error Interno. Ocurrió un error interno.                                                                      | application/json |
| 502    | Puerta de enlace incorrecta. Error inesperado de comunicación.                                                | application/json |

## Ejemplo (formato XML)

cURL

``` bash
curl \
https://sandbox.api.cincel.digital/v3/timestamps/2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e.bitcoin
```

Respuesta (200)

``` xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<node value="5e92ec09501a5d39e251a151f84b5e2228312c445eb23b4e1de6360e27bad54b" type="key">
    <left value="7811e3130908fd2678eb2dd3928d245db7f3ed578c21f2ae4fbe680424dc735e" type="mesh">
        <left value="61f48c118002c0e7681e425a3b0e2396475fe0d037ebb0360231b95c2fd60c2f" type="mesh">
            <left value="54280d75669fb9f3ff976c66d03daefa02b691f1aa25558a85f86a5c60961c69" type="mesh">
                <left value="43e89c93c4247511a6eabfe24dfcd331453e04bdde13e688865bf73c3243280a" type="mesh"/>
                <right value="6c127084785466791190c1b4673635274d21e065b9ad9c70d246ef41ba960220" type="mesh">
                    <left value="34789458c3010230cef61b6053626ee25f1d79762a76de3517ec5ac2e76ae2e9" type="mesh"/>
                    <right value="f375cd66a5552b189a2bfe5f8be433236ff6829dea7bca710e611e98bc7baf0e" type="mesh">
                        <left value="5e8d9839b88965971e8f8eb06d2d6bf2f72f75b16100ea175350179c6a82807e" type="mesh">
                            <left value="55ed3f4915ab9f93e385f655a2c58c62a5fa2d0e7a4b741263102184e2b7a94f" type="mesh">
                                <left value="c04c5bb97b65d009582feb32d88498880216aee8e3bda0c5a0f4ba12244aedb0" type="mesh"/>
                                <right value="0bee8d27b7fd9129df599166751af1a9e82bf4e7ac91771f9c15ebf0cb05d74d" type="mesh">
                                    <left value="88369cb8594af97fb2ee465d5dc3a01e1e23924b8929faca6fdd7293f97e92be" type="mesh">
                                        <left value="d7ed34b6b74056df3cfb2836bfd55c4cd27b5237ad4a6641cf7003d21be76c88" type="mesh"/>
                                        <right value="af68501bb80cca4c0bc7d4df6aa2a0c347712e1f2cb1b687a0467e2e9aaf545e" type="mesh">
                                            <left value="debd8f8a4aa7af602faa9e8e08f5773c6decc29c9f65e80d70ee90a993170f69" type="mesh">
                                                <left value="8a477ae8ff3b601e709321b17f00117159a7efd861abc74286f7ee6479fe46ce" type="mesh">
                                                    <left value="6a2368efa74c641f2f32d6be8f90a7b6ab5a3235d816faf4034d53d49ce5537c" type="mesh"/>
                                                    <right value="2796b0a5986fe8f497057da457cdcf51ab9fc0ffb996445434bd08e2257b29e8" type="mesh">
                                                        <left value="2c5838c92697fe3d5b8aa14e8f67f6fd2ff4c9e12d536332f57434ebcf0f0196" type="mesh"/>
                                                        <right value="2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e" type="hash"/>
                                                    </right>
                                                </left>
                                                <right value="3d2466ee2746f818708f8f396649e0cc4d13738248e8ee051a767474ac2f87d3" type="mesh"/>
                                            </left>
                                            <right value="130921ae3e04ea13d237a9ed09e33c18574068e8db52e3d7d3d4855d7df3b3ce" type="mesh"/>
                                        </right>
                                    </left>
                                    <right value="8a5d691e559e95448c9aab2d92ba6d58ec9f814ee345e10aee81cf47790d7c2e" type="mesh"/>
                                </right>
                            </left>
                            <right value="55562de568950252020816b6204cbc76820c7c9874c2a4672c99437d99068cf5" type="mesh"/>
                        </left>
                        <right value="f5fef74b0d216ca6221b024cc5f53d5aee7c9bc33f7289c529f465a0ff2969ea" type="mesh"/>
                    </right>
                </right>
            </left>
            <right value="584d06e48bbfe9c5319495b7b3be9eaf7e11aa546ae777575e7e422aafa68fcd" type="mesh"/>
        </left>
        <right value="bf11a94cc0028fe2e0fe476d897da3417cc72777e523d6f0c030c5141bbeb75b" type="mesh"/>
    </left>
    <right value="a62eedb07080ce5a21ad26230bcd50ef37cac8cca43a2f1d946db2d1d47e1f94" type="mesh"/>
</node>
```

## Generación de certificados PDF para Bitcoin

Además del certificado en formato XML, puedes generar un certificado en formato PDF que incluye todos los datos de tu estampa de tiempo. Ambos certificados contienen la misma información; CINCEL te ofrece ambos formatos para tu mayor conveniencia (por ejemplo, el certificado PDF es más vistoso, mientras que el certificado XML es mucho más compacto).

### URL

```
GET https://sandbox.api.cincel.digital/v3/timestamps/{hash}.pdf
```

### Códigos de Respuesta

| Código | Descripción                                                                                                   | Content-Type     |
|:------:|:-------------------------------------------------------------------------------------------------------------:|:----------------:|
| 200    | OK. La petición fue realizada correctamente.                                                                  | application/pdf  |
| 202    | Aceptado. La petición fue admitida correctamente.                                                             | application/json |
| 400    | Petición erronea. Hubo un error en el cuerpo de la petición.                                                  | application/json |
| 401    | No autorizado. La petición no se pudo completar por credenciales incorrectas o no proporcionadas.             | application/json |
| 402    | Pago requerido. El usuario no cuenta con creditos suficientes para realizar la petición.                      | application/json |
| 404    | No encontrado. No se encontró el certificado del hash proporcionado y el usuario no proporciono credenciales. | application/json |
| 500    | Error Interno. Ocurrió un error interno.                                                                      | application/json |
| 502    | Puerta de enlace incorrecta. Error inesperado de comunicación.                                                | application/json |

## Ejemplo (formato PDF)

cURL

``` bash
curl \
https://sandbox.api.cincel.digital/v3/timestamps/2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e.pdf
```

Respuesta (200)

![Ejemplo Certificado Blockchain parte 1](/img/cert-example-pdf-1.jpg "Ejemplo Certificado Blockchain parte 1")
![Ejemplo Certificado Blockchain parte 2](/img/cert-example-pdf-2.jpg "Ejemplo Certificado Blockchain parte 2")
