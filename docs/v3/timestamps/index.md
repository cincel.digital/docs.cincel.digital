---
title: Estampas de tiempo
sidebar_label: Descripción general
slug: /v3/timestamps
---

En CINCEL ofrecemos el servicio de estampado de archivos digitales en la blockchain de [Bitcoin](/v3/timestamps/bitcoin) y estampado [NOM 151](/v3/timestamps/nom151).

Para poder utilizar el servicio de estampas de tiempo es necesario que:

- Obtengas tu token de acceso para la API v3 de CINCEL o cuentes con tu correo y contraseña de CINCEL
- Calcules el hash (SHA256) del archivo digital que deseas estampar
- Tengas créditos disponibles en tu cuenta CINCEL

:::info
Ejemplo de hash SHA256:
`2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e`
:::

### URL

```
GET https://sandbox.api.cincel.digital/v3/timestamps/{hash}
```

### Parámetros

| Parámetro          | Tipo de Dato | Descripción                                                                 |
|:------------------:|:------------:|:---------------------------------------------------------------------------:|
| _hash_ (requerido) | _string_     | El hash SHA256 de tu archivo.<br />Exactamente 32 caracteres hexadecimales. |

### Autenticación

- Bearer Token (opcional)
- Basic Auth (opcional)

### Códigos de Respuesta

| Código | Descripción                                                                                       | Content-Type     |
|:------:|:-------------------------------------------------------------------------------------------------:|:----------------:|
| 300    | Multiples opciones. Un listado de las estampas de tiempo generadas.                               | application/json |
| 400    | Petición erronea. Hubo un error en el cuerpo de la petición.                                      | application/json |
| 401    | No autorizado. La peticion no se pudo completar por credenciales incorrectas o no proporcionadas. | application/json |
| 500    | Error Interno. Ocurrio un error interno.                                                          | application/json |

## Ejemplo

cURL

``` bash
curl \
https://sandbox.api.cincel.digital/v3/timestamps/2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e
```

Respuesta (300)

``` bash
< HTTP/2 300
⋮
< location: 2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e.bitcoin
```

``` json
[{
    "type": "bitcoin",
    "xml": "2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e.bitcoin",
    "pdf": "2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e.pdf"
},
{
    "type": "ethereum",
    "xml": "2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e.ethereum"
},
{
    "type": "nom151",
    "asn1": "2c5d36be542f8f0e7345d77753a5d7ea61a443ba6a9a86bb060332ad56dba38e.asn1"
}]
```

:::tip
El mismo crédito de estampa de tiempo te da derecho a generar todas las estampas de tiempo que ofrece CINCEL, sin costo adicional:

- [Estampa de tiempo Bitcoin](/v3/timestamps/bitcoin) 
- Estampa de tiempo Ethereum
- [Estampa de tiempo NOM 151](/v3/timestamps/nom151)
:::
