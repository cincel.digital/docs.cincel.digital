---
id: jwt
title: Generación de token JWT
---

Para todas las funcionalidades v3 de la API es necesario solicitar un token de autenticación (jwt).

Para ello se requieren las credenciales que se usan para acceder a [app.cincel.digital](https://app.cincel.digital/).

:::info
Los tokens JWT generados desde v3 NO son válidos para acceder a la API v2. Si requieres generar un token para acceder a la API v2, visita la [guia de v2](/v2/auth/authentication_2).
:::

## URL

```
GET https://sandbox.api.cincel.digital/v3/tokens/jwt
```

## Autenticación

- Basic Auth

## Códigos de Respuesta

| Código | Descripción                                                                                       | Content-Type     |
|:------:|:-------------------------------------------------------------------------------------------------:|:----------------:|
| 200    | OK. La petición fue realizada correctamente.                                                      | plain/text       |
| 400    | Petición errónea. Hubo un error en el cuerpo de la petición.                                      | application/json |
| 401    | No autorizado. La petición no se pudo completar por credenciales incorrectas o no proporcionadas. | application/json |

## Ejemplo

Petición

```
curl --location --request GET 'https://sandbox.api.cincel.digital/v3/tokens/jwt' \
--header 'Authorization: Basic dGVzdGluZ0BjaW5jZWwuZGlnaXRhbDowMTkyODM3NDY1'
```

Respuesta

```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjk5OTk5OTk5OTk5OSwic2NvcGUiOltdLCJpYXQiOjE2Mz
```
