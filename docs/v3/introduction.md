---
id: introduction
title: CINCEL v3
sidebar_label: Introducción
slug: /v3
---

La API v3 de CINCEL está diseñada basado en el esquema [RESTful API](https://restfulapi.net/). Esto permite que las integraciones de terceros puedan realizarse de una forma mas rapida y eficiente al solo seguir los patrones REST.

Actualmente, con la API v3 puedes realizar las siguientes tareas:

- Generación de tokens de acceso
- Generación de certificados NOM-151
- Generación de estampas de tiempo blockchain
- Generación de certificados blockchain (PDF)

:::tip
Si deseas realizar alguna otra opción no mencionada aquí, te invitamos a consultar la [documentación de la API v2](/v2).
:::
