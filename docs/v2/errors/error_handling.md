---
id: error_handling
title: Manejo de errores
---

Al interactuar con los diferentes endpoints de la API, pueden ocurrir una serie de errores dependiendo de diferentes situaciones como enviar datos en formato incorrecto, parámetros inválidos, etc.

En Cincel manejamos un estándar propio para las posibles respuestas de todos los endpoints. Dicha respuesta está conformada por 3 variables principales: ``ok``, ``payload`` y ``errors``.

Si la llamada se realizó de manera exitosa la variable ``ok`` tendrá un valor de ``true`` y dentro de ``payload`` encontrarás el contenido esperado por la llamada realizada, la cuál se especifica en cada una de las llamadas. 

Respuesta exitosa:

```json
{
    "ok": true,
    "payload": {
      ...
    }
}
```

Por el contrario si ocurré un problema, la variable ``ok`` tendrá un valor de ``false`` y ``errors`` contendrá información sobre el error o errores detectados.

El formato de los errores está definido de la siguiente manera:

```json
{
    "ok": false,
    "errors":{
      "variable_1": ["error 1","error n"],
      "arreglo_1": {
        "index_elemento": {
          "variable_1_elemento_1":["error 1","error n"]
        }
      }
    }

}
```

Tanto las respuestas exitosas como los errores conocidos, regresarán un status ``200``, en caso de que ocurra un error inesperado (lo cuál es raro), se regresará un status ``500``.
