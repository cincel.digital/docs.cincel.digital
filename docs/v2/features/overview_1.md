---
id: overview_1
title: Introducción
sidebar_label: API v2
slug: /v2
---

Nuestra API v2 está implementada inspirado en el esquema API REST, por lo que todas las llamadas intercambian mensajes en formato JSON.

Con el uso de nuestra API v2 puedes realizar las siguientes tareas:

- Creación de documentos.
- Eliminación de documentos.
- Obtener la lista de tus documentos.
- Obtener un documento en específico.
- Enviar recordatorios de firma.

Si deseas consultar las guias de la API v3, puedes revisar la [documentación de la API v3](/v3).
