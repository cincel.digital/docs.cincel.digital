---
id: list_by_date_range
title: Lista de documentos por rango de fecha
---

Para obtener la lista de tus documentos filtrados por un rango de fecha (``from_date`` y ``to_date``) y status (``status``), basta con realizar un ``GET`` a ``/documents/by_date_range``, el cual regresará un arreglo con las propiedades principales de cada documento. También es posible pasarle ciertas propiedades opcionales para obtener solo los documentos de una carpeta u organización.

Esto es lo que puede aceptar status:

```js
status = ["signed","unsigned","partially_signed"]
```

Ejemplo:

```bash
curl  --location --request GET 'https://sandbox.api.cincel.digital/v1/documents/by_date_range?status=signed&to_date=2022-12-30' \
      --header 'x-api-key: my_secret_key' \
      -d status=signed
      -d from_date=2021-01-01
      -d to_date=2022-12-30
```

Respuesta:

```json
{
    "ok": true,
    "payload": [
        {
            "id": 15897,
            "name": "Document",
            "description": "Document description",
            "created_at": "2021-01-20T20:30:25.229-06:00",
            "status": "signed",
            "unsigned_last_version_url": "https://s3.amazonaws.com/docs.cincel.digital/...",
            "signed_last_version_url": "https://s3.amazonaws.com/docs.cincel.digital/...",
            "last_version_signed_ended_at": "2021-01-20T20:30:41.586-06:00",
            "organization_id": null,
            "organization_name": null,
            "created_by_id": 537,
            "created_by_name": "Servicios Cincel",
            "created_by_email": "servicios@cincel.digital"
        }
        ...
    ]
}
```

Ten en cuenta que habrá documentos que no pertenezcan a ninguna carpeta u organización, por lo que las propiedades de ``organization`` y ``folder_id`` pueden ser ``null``, así como las demás propiedades opcionales.
