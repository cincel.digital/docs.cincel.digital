---
id: list
title: Lista de documentos
---

Para obtener la lista de tus documentos, basta con realizar un ``GET`` a ``/documents``, el cual regresará un arreglo con las propiedades principales de cada documento. También es posible pasarle ciertas propiedades opcionales para obtener solo los documentos de una carpeta u organización.

Ejemplo:

```bash
curl  --location --request GET 'https://sandbox.api.cincel.digital/v1/documents?organization_id=0&folder_id=0' \
      --header 'x-api-key: my_secret_key'
```

Respuesta:

```json
{
    "ok": true,
    "payload": [
        {
            "id": 0,
            "owner_id": 0,
            "owner_type": "User",
            "folder_id": 0,
            "name": "Contrato 1",
            "description": "Contrato para adquisición de bienes",
            "last_version": "1596141864",
            "created_at": "2020-01-01T00:00:00.000-05:00",
            "updated_at": "2020-01-01T00:00:00.000-05:00",
            "status": "unsigned",
            "organization": {
                "id": 0,
                "name": "Cincel"
            }
        }
        ...
    ]
}
```

Ten en cuenta que habrá documentos que no pertenezcan a ninguna carpeta u organización, por lo que las propiedades de ``organization`` y ``folder_id`` pueden ser ``null``, así como las demás propiedades opcionales.
