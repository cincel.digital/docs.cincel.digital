---
id: delete
title: Eliminar documento
---

Para eliminar un documento, basta con realizar un ``DELETE`` a ``/documents/:document_id``

Ejemplo:

```bash
curl  --location --request GET 'https://sandbox.api.cincel.digital/v1/documents/0' \
      --header 'x-api-key: my_secret_key'
```

Respuesta:

```json
{
  "ok": true,
  "payload": {}
}
```

Ten en cuenta que una vez eliminado el documento no podrás recuperarlo y tanto tú como los firmantes no podrán acceder a el.
