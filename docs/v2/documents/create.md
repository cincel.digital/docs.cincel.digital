---
id: create
title: Crear documento
---

Para poder crear un documento se requiere hacer un ``POST`` a ``/documents``, proporcionando el archivo pdf en base64, un título, la lista de firmantes y una serie de otros parámetros opcionales.

Ejemplo:

```bash
curl --location --request POST 'https://sandbox.api.cincel.digital/v1/documents'\
  --header 'x-api-key: my_secret_key' \
  --header 'Content-Type: application/json' \
  --data-raw '{
      "name":"Contrato 1",
      "description":"Contrato para adquisición de bienes",
      "invitations_attributes":[
        {
          "invite_email":"juan@example.com",
          "invite_name":"Juan Pérez",
          "message":"Por favor firma el contrato de adquisición de bienes.",
          "allowed_signature_types": "all",
          "stage": 2
        },	
        {
          "invite_email":"ana@example.com",
          "invite_name":"Ana Pérez",
          "message":"Por favor firma el contrato de adquisición de bienes.",
          "allowed_signature_types": "autograph",
          "stage": 1
        },	
        {
          "invite_email":"cecilia@example.com",
          "invite_name":"Cecilia Pérez",
          "message":"Por favor firma el contrato de adquisición de bienes.",
          "allowed_signature_types": "advanced",
          "stage": 1
        }
      ],
      "base64_file":"data:application/pdf;base64,JVBERi0xLjMKJf////..."
  }'
```

Ten en cuenta que al utilizar tu clave productiva estarás limitado por el número de documentos que hayas adquirido y el espacio de almacenamiento que tengas disponible en tu cuenta.

Al crear un documento no se disminuirá tu contador de documentos disponibles de forma inmediata, sino hasta que el proceso de firmado haya iniciado cuando alguno de los firmantes firme el documento.
