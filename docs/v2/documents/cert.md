---
id: cert
title: Obtener certificados blockchain para descargar
---

Para descargar los certificados blockchain se debe acceder a todas las propiedades de un documento, es necesario obtenerlo mediante su ``id``, realizando un ``GET`` a ``/documents/:document_id`` ahí vamos a validar las siguientes variable: `xml_btc_cert`, `xml_eth_cert` y `sha256_signed_file` estas variables se encuentran dentro del atributo `file {}`, una vez validado que las tres variables tiene valores se generara la url de descarga:

Ejemplo de ruta final para descarga de cada certificado:

BTC:
`https://sandbox.api.cincel.digital/v3/timestamps/:sha256_signed_file+.pdf`

ETH:
`https://sandbox.api.cincel.digital/v3/timestamps/eth/:sha256_signed_file+.pdf`

```bash
curl  --location --request GET 'https://sandbox.api.cincel.digital/v1/documents/0' \
  --header 'x-api-key: my_secret_key' \
  --header 'Content-Type: application/json' \
```

Respuesta cuando los certificados están listos:

```bash {39,40,42}
{
    "ok": true,
    "payload": {
        "id": 61561,
        "with_nom151": true,
        "owner_id": 1616,
        "owner_type": "User",
        "folder_id": null,
        "name": "p_seguridata",
        "description": "p_seguridata",
        "last_version": "1637938962",
        "creator_id": 1616,
        "advanced_signature_version": "seguridata_v1",
        "autograph_signature_version": "seguridata_v1",
        "created_at": "2021-11-26T09:02:41.553-06:00",
        "updated_at": "2021-11-26T09:03:32.739-06:00",
        "status": "signed",
        "active": true,
        "validate_signers_3id": false,
        "blockchain_certs_enabled": true,
        "signing_stage": 1,
        "organization": null,
        "files": [
            {
                "id": 58558,
                "document_id": 61561,
                "name": null,
                "signed_ended_at": "2021-11-26T09:03:32.720-06:00",
                "status": "signed",
                "sha256": "4FB8FEDC1354D145E7D895EE87D34787C6D4A71573CCD0F9C24FE25A1D9B893D",
                "adv_id": null,
                "adv_moment": null,
                "adv_str_end": null,
                "adv_str_now": null,
                "adv_str_constance": "MIIPwDADAgEAMIIPtwYJKoZIhvcNAQcCoIIPqDCCD6QCAQMxDzANBglghkg....",
                "version": "1637938962",
                "created_at": "2021-11-26T09:02:42.700-06:00",
                "updated_at": "2021-11-26T19:02:20.782-06:00",
                "xml_btc_cert": "\u003c?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"y.....",
                "xml_eth_cert": "\u003c?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?\u.....",
                "blockchain_certs_status": "sended",
                "sha256_signed_file": "b481d040d9f90f7d82ce90473538d61904f18eefba0c549dc89c9047db1a35ce",
                "url": "https://s3.amazonaws.com/docs.cincel.digital/iaG8UoLHp6vfUS1613ag3641?...",
                "signed_file_url": "https://s3.amazonaws.com/docs.cincel.digital/wYeshNNoD6hr6rWVzrjicjvZ?...",
                "evidence_doc_url": null,
                "comments": [],
                "signers": [
                    {
                        "id": 78568,
                        "file_id": 58558,
                        "signature_type": "advanced",
                        "created_at": "2021-11-26T09:03:03.910-06:00",
                        "updated_at": "2021-11-26T09:03:20.817-06:00",
                        "status": "signed",
                        "email": "hector@cincel.digital",
                        "hash_to_sign": "BAFEF72BB2090DF68C945CEB3752B33D833733C5906F323E7898DB904EAC81D4",
                        "user": {
                            "email": "hector@cincel.digital",
                            "name": "Hector Gutierrez",
                            "user_id": 1616
                        },
                        "user_id": 1616
                    },
                    {
                        "id": 78567,
                        "file_id": 58558,
                        "signature_type": "advanced",
                        "created_at": "2021-11-26T09:03:03.867-06:00",
                        "updated_at": "2021-11-26T09:03:30.233-06:00",
                        "status": "signed",
                        "email": "amg@cincel.digital",
                        "hash_to_sign": "13473CCA22633AF6E63E74A060E5811E532BC4D5A90244F2C836AD9952A3612F",
                        "user": {
                            "email": "amg@cincel.digital",
                            "name": "Amelia Gomez",
                            "user_id": 4014
                        },
                        "user_id": 4014
                    }
                ]
            }
        ],
        "invitations": [
            {
                "id": 78541,
                "invite_name": "Hector Gutierrez",
                "invite_email": "hector@cincel.digital",
                "document_id": 61561,
                "status": "accepted",
                "created_at": "2021-11-26T09:03:03.882-06:00",
                "updated_at": "2021-11-26T09:03:03.882-06:00",
                "stage": 1,
                "customer_id_3id": null,
                "validation_status_3id": "pending",
                "allowed_signature_types": "all",
                "requested_documents_status": "pending",
                "requested_documents_required": false,
                "validate_kyc": false,
                "requested_documents": []
            },
            {
                "id": 78540,
                "invite_name": "Amelia Gomez",
                "invite_email": "amg@cincel.digital",
                "document_id": 61561,
                "status": "accepted",
                "created_at": "2021-11-26T09:03:03.849-06:00",
                "updated_at": "2021-11-26T09:03:30.246-06:00",
                "stage": 1,
                "customer_id_3id": null,
                "validation_status_3id": "pending",
                "allowed_signature_types": "autograph",
                "requested_documents_status": "pending",
                "requested_documents_required": false,
                "validate_kyc": false,
                "requested_documents": []
            }
        ]
    }
}
```

Respuesta cuando los certificados no están listos:

```bash {39,40}
{
    "ok": true,
    "payload": {
        "id": 6310,
        "with_nom151": true,
        "owner_id": 3,
        "owner_type": "User",
        "folder_id": null,
        "name": "Documento que no tiene listo los documentos",
        "description": "Descripción de documento",
        "last_version": "1622672918",
        "creator_id": 3,
        "advanced_signature_version": "seguridata_v1",
        "autograph_signature_version": "seguridata_v1",
        "created_at": "2021-06-02T17:28:34.784-05:00",
        "updated_at": "2021-06-10T09:46:40.180-05:00",
        "status": "signed",
        "active": true,
        "validate_signers_3id": false,
        "blockchain_certs_enabled": true,
        "signing_stage": 1,
        "organization": null,
        "files": [
            {
                "id": 5986,
                "document_id": 6310,
                "name": null,
                "signed_ended_at": "2021-06-10T09:46:40.167-05:00",
                "status": "signed",
                "sha256": "04C67C37510CB5D6942C207CDF6000E1F10D1B4A686F5945856E5A4DB8B3B973",
                "adv_id": null,
                "adv_moment": null,
                "adv_str_end": null,
                "adv_str_now": null,
                "adv_str_constance": "MIIINDADAgEAMIIIKwYJKoZIhvcNAQcCoIIIHDCCCBgCAQMxDzANBglghkgBZQMEAgEFA....",
                "version": "1622672918",
                "created_at": "2021-06-02T17:28:38.807-05:00",
                "updated_at": "2021-06-10T09:46:41.082-05:00",
                "xml_btc_cert": null,
                "xml_eth_cert": null,
                "blockchain_certs_status": "requested",
                "sha256_signed_file": "eb82025d1d4e6fc86f66a9ea1b53ff5e46b63c73ebe673193d8387ae36a5f1c1",
                "url": "https://s3.amazonaws.com/staging.docs.cincel.digital/jeFZfB31qLhb...",
                "signed_file_url": "https://s3.amazonaws.com/staging.docs.cincel.digital/4f8xRnr7e...",
                "evidence_doc_url": null,
                "comments": [],
                "signers": [
                    {
                        "id": 7030,
                        "file_id": 5986,
                        "signature_type": "advanced",
                        "created_at": "2021-06-02T17:28:39.480-05:00",
                        "updated_at": "2021-06-10T09:46:39.117-05:00",
                        "status": "signed",
                        "email": "ferr@cincel.digital",
                        "hash_to_sign": "186FAFAAFCF9A11BB178939567D596DC04B726979998610973FF33C8CCCE9208",
                        "user": {
                            "email": "fer@cincel.digital",
                            "name": "Fernando",
                            "user_id": 3
                        },
                        "user_id": 3
                    }
                ]
            }
        ],
        "invitations": [
            {
                "id": 7035,
                "invite_name": "Fernando",
                "invite_email": "fer@cincel.digital",
                "document_id": 6310,
                "status": "accepted",
                "created_at": "2021-06-02T17:28:39.452-05:00",
                "updated_at": "2021-06-02T17:28:39.452-05:00",
                "stage": 1,
                "customer_id_3id": null,
                "validation_status_3id": "pending",
                "allowed_signature_types": "all",
                "requested_documents_status": "pending",
                "requested_documents_required": false,
                "validate_kyc": false,
                "requested_documents": []
            }
        ]
    }
}
```
