---
id: standar
title: Crear odocumento estandar
---

Para poder crear una organización se requiere hacer un ``POST`` a ``/documets``, proporcionando el archivos pdf en base64, un nombre, la lista de firmantes y una serie de parámetros. Para asegurar la creación del documento estandar se deben mandar las siguientes tres variables cn los respectivos valores:

``autograph_signature_version = 'cincel_v1'``
``advance_signature_version = 'cincel_v1'``
``with_nom151 = false``


```bash
curl --location --request POST 'https://sandbox.api.cincel.digital/v1/documents'\
  --header 'x-api-key: my_secret_key' \
  --header 'Content-Type: application/json' \
  --data-raw '{
     {
    "name":"Contrato 2 APi",      
    "description":"Contrato para adquisición de bienes2",
    "invitations_attributes":[
        {
            "invite_email":"pruebas@cincel.digital",
            "invite_name":"Juan Pérez",
            "message":"Por favor firma el contrato de adquisición de bienes.",
            "allowed_signature_types": "all",
            "stage": 2
        },
        {
            "invite_email":"ana@example.com",
            "invite_name":"Ana Pérez",
            "message":"Por favor firma el contrato de adquisición de bienes.",
            "allowed_signature_types": "autograph",          "stage": 1
        },
        {
            "invite_email":"cecilia@example.com",
            "invite_name":"Cecilia Pérez",
            "message":"Por favor firma el contrato de adquisición de bienes.",
            "allowed_signature_types": "advanced",
            "stage": 1
        }
    ],
    "with_nom151": false,
    "autograph_signature_version": "cincel_v1",
    "advanced_signature_version": "cincel_v1",
    "base64_file":"data:application/pdf;base64,JVBERi0xLjMKJcTl8uXrp/Og0MTGCjQ..."  }
  }'
```


Ten en cuenta que al utilizar tu clave productiva estarás limitado por el número de documentos que hayas adquirido y el espacio de almacenamiento que tengas disponible en tu cuenta.
