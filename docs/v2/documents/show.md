---
id: show
title: Obtener documento
---

Para poder acceder a todas las propiedades de un documento, así como su lista de versiones y firmantes es necesario obtenerlo mediante su ``id``, realizando un ``GET`` a ``/documents/:document_id``.

Ejemplo:

```bash
curl  --location --request GET 'https://sandbox.api.cincel.digital/v1/documents/0' \
      --header 'x-api-key: my_secret_key'
```

Respuesta:

```bash
{
    "ok": true,
    "payload": {
        "id": 21,
        "owner_id": 2,
        "owner_type": "User",
        "folder_id": null,
        "name": "Contrato 1",
        "description": "Contrato de adquisición de bienes",
        "last_version": "1596125425",
        "created_at": "2020-01-01T00:00:00.000-05:00",
        "updated_at": "2020-01-01T00:00:00.000-05:00",
        "status": "signed",
        "organization": null,
        "files": [
            {
                "id": 0,
                "document_id": 0,
                "name": null,
                "signed_ended_at": "2020-01-01T00:00:00.000-05:00",
                "status": "signed",
                "sha256": "CD4E31AC2288FDAA752E37FAB847E53FC4D0734010F193580C2790C9660D6A90",
                "version": "1596125425",
                "created_at": "2020-01-01T00:00:00.000-05:00",
                "updated_at": "2020-01-01T00:00:00.000-05:00",
                "url": "URL_DEL_DOCUMENTO_PDF_ORIGINAL",
                "signed_file_url": "URL_DEL_DOCUMENTO_PDF_FIRMADO",
                "comments": [
                    {
                        "title": "Mi comentario",
                        "text": "comentario 1",
                        "created_at": "2020-01-01T00:00:00.000-05:00",
                        "user": {
                            "name": "Juan Pérez",
                            "email": "example@mail.com"
                            "id": null,
                        }
                    }
                ],
                "signers": [
                    {
                        "id": 0,
                        "file_id": 0,
                        "signature_type": "advanced",
                        "created_at": "2020-01-01T00:00:00.000-05:00",
                        "updated_at": "2020-01-01T00:00:00.000-05:00",
                        "status": "signed",
                        "email": "example@mail.com",
                        "user": {
                            "email": "example2@mail.com",
                            "name": "Pedro Pérez",
                            "user_id": 0
                        }
                    }
                ]
            }
        ],
        "invitations": [
            {
                "id": 0,
                "invite_name": "Juan Pérez",
                "invite_email": "example@mail.com",
                "document_id": 0,
                "status": "accepted",
                "created_at": "2020-01-01T00:00:00.000-05:00",
                "updated_at": "2020-01-01T00:00:00.000-05:00",
                "user": {
                    "user_id": null,
                    "name": "Juan Pérez",
                    "email": "example@mail.com"
                },
            }
        ]
    }
}
```

Como podemos observar el documento esta conformado por una serie de objetos que representan, su lista de versiones, su lista de invitaciones a firmar, comentarios y firmantes de cada versión. Nos enfocaremos solo en las mas relevantes.

### ``Document['files']``

Existe la posibilidad de crear nuevas versiones para un documento, en caso de que éste contenga errores y se requiera subir un nuevo PDF, sin tener que invitar nuevamente a los firmantes. Dentro de la API hacemos referencia a esas versiones como ``files``. Cabe mencionar que solo se permite la creación de nuevas versiones para documentos que tengan status ``unsigned``, es decir, que ninguno de los firmantes haya firmado previamente. Por el momento la creación de versiones solo se encuentra disponible dentro de la plataforma de CINCEL.

La propiedad ``files`` es un arreglo de objetos donde cada uno contiene dentro la url del pdf original y del pdf firmado. Así como la lista de comentarios realizados por los diferentes usuarios que fueron invitados a firmar el documento y que difieren entre cada versión del documento. Por el momento solo se permite comentar a usuarios registrados dentro de CINCEL.

Para identificar la última versión de un documento podemos apoyarnos de la variable ``last_version`` del documento la cual debe coincidir con la propiedad ``version`` de alguno de los elementos de ``files``.

### ``Document['invitations']``

La lista de firmantes que se incluyeron en la creación del documento esta representada por la propiedad ``invitations`` la cual es un arreglo de objetos, con el nombre y correo del firmante invitado. Así como información de su usuario, en caso de que el correo pertenezca a un usuario registrado en CINCEL.
