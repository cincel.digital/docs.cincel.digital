---
id: create
title: Crear carpeta
---

Para poder crear una carpeta se requiere hacer un ``POST`` a ``/folders``, proporcionando el nombre de la carpeta.

```bash
curl --location --request POST 'https://sandbox.api.cincel.digital/v1/folders'\
  --header 'x-api-key: my_secret_key' \
  --header 'Content-Type: application/json' \
  --data-raw '{
      "name":"Carpeta de contratos"
  }'
```

Respuesta:

```bash
{
    "ok": true,
    "payload": {
        "name": "Carpeta de contratos",
        "id": 235,
        "owner_type": "User",
        "owner_id": 3,
        "creator_id": 3,
        "created_at": "2021-12-13T09:33:50.985-06:00",
        "updated_at": "2021-12-13T09:33:50.985-06:00"
    }
}

```
