---
id: create
title: Crear organización
---

Para poder crear una organización se requiere hacer un ``POST`` a ``/organizations``, proporcionando el nombre de la organización y una imagen en base64, con extensión .png.

```bash
curl --location --request POST 'https://sandbox.api.cincel.digital/v1/documents'\
  --header 'x-api-key: my_secret_key' \
  --header 'Content-Type: application/json' \
  --data-raw '{
      "name":"Organización Cincel",
      "imagotype":"Cdata:image/png;base64, iVbORw0KGgoAAAANSUhEUgAAAN///..",
  }'
```
