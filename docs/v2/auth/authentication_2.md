---
id: authentication_2
title: Método de autenticación
---

Para poder autenticarte debes incluir la API KEY en el header X-Api-Key de la siguiente forma:

```
X-Api-Key = "XX-XXX-XXXXXX"
```
