---
id: authentication_1
title: Cómo obtener tus claves de API
---

### Requisitos previos
- Tener una cuenta en [CINCEL](https://sandbox.app.cincel.digital/).
- Conocer tu correo y contraseña de CINCEL.

### 1. Genera un JsonWebtoken

Genera un JWT de la siguiente forma, reemplazando el correo electrónico y password por los de tu cuenta CINCEL.

```bash
curl 'https://sandbox.api.cincel.digital/v1/login' \
-H 'Content-Type: application/json;charset=utf-8' \
--data-raw '{"email":"user@example.com","password":"contraseña"}' 
```

Obtendrás una respuesta como la que se muestra a continuación, `token` contiene el JWT.

```javascript
{
  "token": "eyJhbGciOiJIUzI1NiJ9._._",
  "expiration": 1603915806
}
```

### 2. Obtén tu APIKEY

Usando el JWT que obtuviste en el paso anterior, envía la siguiente petición GET a nuestra API, para obtener tu APIKEY:

```bash
curl 'https://sandbox.api.cincel.digital/v1/users/api_key' \
-H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9._._'
-H 'Content-Type: application/json;charset=utf-8' \
```

El cuerpo de la respuesta contiene tu APIKEY.

```javascript
{
  "ok": true,
  "payload": {
    "api_key": "Q-f89hj9s08hsd_u"
  }
}
```

Para obtener tus claves de autenticación, primero debes iniciar sesión en tu cuenta de [cincel](https://app.cincel.digital/login).

Debes navegar a la sección de "Mi Perfil" en la esquina superior derecha, una vez dentro busca un botón que diga "Generar API Keys". Te guiará por el proceso para obtener las claves con las cuales podrás consumir nuestra API.
